package com.recipe.ido.recipeworldfinal.model;

import android.os.AsyncTask;
import android.util.Log;

import com.recipe.ido.recipeworldfinal.data.Book;

import java.util.LinkedList;
import java.util.List;

public class BookAsynchDao {
    static int lastReplacedListSize = 0;
    static boolean isInserting = false;
    interface BookAsynchDaoListener<T>{
        void onComplete(T data);
    }
    static public void getAll(final BookAsynchDaoListener<List<Book>> listener) {
        Log.d("dao", "get all dao");

        class MyAsynchTask extends AsyncTask<String,String,List<Book>> {
            @Override
            protected List<Book> doInBackground(String... strings) {
                List<Book> stList = AppLocalDb.db.bookDao().getAll();
                return stList;
            }

            @Override
            protected void onPostExecute(List<Book> students) {
                super.onPostExecute(students);
                listener.onComplete(students);
            }
        }
        MyAsynchTask task = new MyAsynchTask();
        task.execute();
    }

    private static void deleteAll(final BookAsynchDaoListener<Boolean> listener) {
        class MyAsynchTask extends AsyncTask<String,String,List<Book>> {
            @Override
            protected List<Book> doInBackground(String... strings) {
                Log.d("dao", "Delete dao");
                AppLocalDb.db.bookDao().deleteAllBooks();
                return null;
            }

            @Override
            protected void onPostExecute(List<Book> students) {
                super.onPostExecute(null);
                listener.onComplete(null);
            }
        }
        MyAsynchTask task = new MyAsynchTask();
        task.execute();
    }
    static void replaceAll(final List<Book> books, final BookAsynchDaoListener<Boolean> listener){
        if (books.size() == lastReplacedListSize) { // if the same size. so nothing new to save...
            listener.onComplete(true);
        } else {
            deleteAll(new BookAsynchDaoListener() {
                @Override
                public void onComplete(Object data) {

                    insertAll(books, new BookAsynchDaoListener<Boolean>() {
                        @Override
                        public void onComplete(Boolean success) {
                            lastReplacedListSize = books.size();
                            listener.onComplete(success);
                        }
                    });
                }
            });
        }


    }

    static private void insertAll(final List<Book> books, final BookAsynchDaoListener<Boolean> listener) {
        final List<Book> dummyList = new LinkedList<Book>();
        dummyList.addAll(books);
        if (!isInserting) {
            isInserting = true;
            class MyAsynchTask extends AsyncTask<List<Book>,String,Boolean> {
                @Override
                protected Boolean doInBackground(List<Book>... books) {
                    for (Book st:books[0]) {
                        AppLocalDb.db.bookDao().insertAll(st);
                    }
                    return true;
                }

                @Override
                protected void onPostExecute(Boolean success) {
                    super.onPostExecute(success);
                    isInserting = false;
                    listener.onComplete(success);
                }
            }
            MyAsynchTask task = new MyAsynchTask();
            task.execute(dummyList);
        }
    }
}
