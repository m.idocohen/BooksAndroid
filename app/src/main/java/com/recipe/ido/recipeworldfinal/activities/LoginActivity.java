package com.recipe.ido.recipeworldfinal.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.recipe.ido.recipeworldfinal.R;
import com.recipe.ido.recipeworldfinal.data.Book;
import com.recipe.ido.recipeworldfinal.fragments.LoginFragment;

import java.util.List;

public class LoginActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    final private int READ_EXTERNAL_STORAGE_REQUEST = 2;
    private Bundle bundle;
    final int REQUEST_WRITE_STORAGE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

// Write a message to the database
//        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
//        Book tmp;
//        for (int x = 0 ; x< 45; x++) {
//            tmp = new Book(mDatabase.push().getKey(), "test name " + x, "author " + x, "desc", null );
//            tmp.setImageUrl("https://firebasestorage.googleapis.com/v0/b/recipeworld-4bece.appspot.com/o/images%2F1531384565867?alt=media&token=dd7256a5-7257-42ef-894c-2d000442e7bb");
//            mDatabase.child("books").child(tmp.getId()).setValue(tmp);
//
//        }



//        DatabaseReference m2Database = FirebaseDatabase.getInstance().getReference().child("books");

////        m2Database.startAt("").limitToFirst(5).addValueEventListener(new ValueEventListener() {
//        m2Database.orderByChild("order").startAt("jfhjkgbbhbigc").limitToFirst(5).addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                // This method is called once with the initial value and again
//                // whenever data at this location is updated.
//                for (DataSnapshot stSnapshot: dataSnapshot.getChildren()) {
//                    Log.d("LoginTag", "Value is: " + stSnapshot.getValue(Book.class));
//
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError error) {
//                // Failed to read value
//                Log.w("LoginTag", "Failed to read value.", error.toException());
//            }
//        });

        bundle = savedInstanceState;
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, READ_EXTERNAL_STORAGE_REQUEST);
        } else {
            start();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                requestPermissions(new String[]{
                        Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
            }
        }



    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case READ_EXTERNAL_STORAGE_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    start();
                } else {
                    Toast.makeText(this, "This app must read from storage to run.",
                            Toast.LENGTH_SHORT).show();
                }
                return;
            }
            case REQUEST_WRITE_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                }
                return;
            }
        }
    }

    private void start() {
        setContentView(R.layout.activity_login);
        mAuth = FirebaseAuth.getInstance();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);

        if (bundle == null) {
            LoginFragment fragment = new LoginFragment();
            FragmentTransaction tran = getSupportFragmentManager().beginTransaction();
            tran.add(R.id.login_container, fragment);
            tran.commit();
        }
    }

    private void updateUI(FirebaseUser currentUser) {
        if (currentUser != null) {
            startWallActivity();
        }
    }

    public void startWallActivity() {
        Intent recipeListIntent = new Intent(this, WallActivity.class);
        //Flags up so no back stack apply.
        recipeListIntent.setFlags(recipeListIntent.FLAG_ACTIVITY_NEW_TASK | recipeListIntent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(recipeListIntent);
    }
}
