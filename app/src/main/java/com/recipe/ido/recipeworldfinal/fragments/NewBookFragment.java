package com.recipe.ido.recipeworldfinal.fragments;



import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.recipe.ido.recipeworldfinal.R;
import com.recipe.ido.recipeworldfinal.activities.WallActivity;
import com.recipe.ido.recipeworldfinal.data.Book;
import com.recipe.ido.recipeworldfinal.model.Model;


import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewBookFragment extends Fragment {
    public ImageView bookImage;
    final private int REQUEST_CODE_PICTURE = 1;
    private boolean imageChanged = false;

    private View editImage;
    private View fab;
    private View progress;
    private View form;
    private TextView name;
    private TextView description;
    private Button save;
    private Button cancel;
    private Uri selectedImage;
    private String bookId = ""; //need to set this if we edit
    private Button delete;

    public NewBookFragment() {
        // Required empty public constructor
    }

    @Override
    public void onDestroy() {
        showFab(true);
        super.onDestroy();
    }
    private void saveClicked() {
        showProgress(true);
        if (Model.instance.lastTouchedMeme != null) {
            editOldMeme();
        } else {
            savingNewMeme();
        }
        if (getActivity() instanceof WallActivity) {
            ((WallActivity)getActivity()).checkAndSetTitle();
        }
        showProgress(false);
    }

    private void editOldMeme() {
        if (name.getText().toString().isEmpty()) {
            Toast.makeText(getActivity(), "All fields are required.",
                    Toast.LENGTH_SHORT).show();
        } else {
            {
                Book book = new Book(Model.instance.lastTouchedMeme.getId(), name.getText().toString(), Model.instance.currentUserName(),
                        description.getText().toString(),null);
                book.setOrder(Model.instance.lastTouchedMeme.getOrder());
                book.setDate(Model.instance.lastTouchedMeme.getDate());
                Bitmap bm = ((BitmapDrawable)bookImage.getDrawable()).getBitmap();
                Model.instance.addMeme(book, bm);
//                Toast.makeText(getActivity(), "Meme edited successfully.",
//                        Toast.LENGTH_SHORT).show();
                getFragmentManager().popBackStack();
            }
        }
    }

    private void savingNewMeme() {
        if (!imageChanged) {
            Toast.makeText(getActivity(), "No image was selected.",
                    Toast.LENGTH_SHORT).show();
        }
        else if (name.getText().toString().isEmpty()) {
            Toast.makeText(getActivity(), "All fields are required.",
                    Toast.LENGTH_SHORT).show();
        } else {
            Bitmap bm = ((BitmapDrawable)bookImage.getDrawable()).getBitmap();
            Model.instance.addMeme(new Book(bookId, name.getText().toString(), Model.instance.currentUserName(),
                    description.getText().toString(),null), bm);
            Toast.makeText(getActivity(), "Meme saved successfully.",
                    Toast.LENGTH_SHORT).show();
            getFragmentManager().popBackStack();
        }
    }

    private void cancelClicked() {
        getFragmentManager().popBackStack();
    }

    private void deleteClicked() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Model.instance.deleteMeme(Model.instance.lastTouchedMeme.getId());
                getFragmentManager().popBackStack();
            }
        });
        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alertDialogBuilder.setTitle("Delete " + Model.instance.lastTouchedMeme.getName());
        alertDialogBuilder.setMessage("Are you sure you want to delete this?");
        alertDialogBuilder.show();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_book, container, false);

        name = (TextView)view.findViewById(R.id.book_new_name);
        description = (TextView)view.findViewById(R.id.new_description);
        fab = getActivity().findViewById(R.id.fab);
        progress = view.findViewById(R.id.new_book_progress);
        form = view.findViewById(R.id.new_book_form);
        save = (Button)view.findViewById(R.id.save_new_button);
        save.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                saveClicked();
            }
        });
        delete = (Button)view.findViewById(R.id.delete_button);
        if (Model.instance.lastTouchedMeme != null) {
            delete.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteClicked();
                }
            });
        } else {
            delete.setVisibility(View.GONE);
        }

        cancel = (Button)view.findViewById(R.id.cancel_new_button);
        cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelClicked();
            }
        });
        showFab(false);
        bookImage = (ImageView)view.findViewById(R.id.book_image);
        editImage = view.findViewById(R.id.edit_image);
        editImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                addImage();
            }
        });
        Book meme;
        if (Model.instance.lastTouchedMeme != null) {
            meme = Model.instance.lastTouchedMeme;
            name.setText(meme.getName());
            description.setText(meme.getDescription());
            Model.instance.getImage(meme.getImageUrl(), new Model.GetImageListener() {
                @Override
                public void onDone(Bitmap imageBitmap) {
                    bookImage.setImageBitmap(imageBitmap);
                }
            });
        }
        return view;
    }

    public void addImage() {
            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            String pickTitle = "Select or take a new Picture"; // Or get from strings.xml
            Intent chooserIntent = Intent.createChooser(pickPhoto, pickTitle);
            chooserIntent.putExtra
                    (
                            Intent.EXTRA_INITIAL_INTENTS,
                            new Intent[] { takePhotoIntent }
                    );
            startActivityForResult(chooserIntent, REQUEST_CODE_PICTURE);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("New", "Set Title");
        if (Model.instance.lastTouchedMeme != null) {
            ((WallActivity)getActivity()).setMyActionBar("Edit meme");
        } else {
            ((WallActivity)getActivity()).setMyActionBar("New meme");
        }
    }

    private void showFab(Boolean show) {

        if (show) {
            fab.setVisibility(View.VISIBLE);
        } else {
            fab.setVisibility(View.GONE);
        }
    }

    private void showProgress(Boolean show) {
        if (show) {
            progress.setVisibility(View.VISIBLE);
            form.setVisibility(View.GONE);
        } else {
            progress.setVisibility(View.GONE);

            form.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Model.instance.lastTouchedMeme = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case REQUEST_CODE_PICTURE:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = data.getData();
                    bookImage.setImageURI(selectedImage);
                    imageChanged = true;
                }
                break;
        }
    }

}
