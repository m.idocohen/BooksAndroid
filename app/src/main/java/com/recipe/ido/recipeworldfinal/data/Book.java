package com.recipe.ido.recipeworldfinal.data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.google.firebase.database.Exclude;
import com.recipe.ido.recipeworldfinal.model.Model;

@Entity
public class Book {
    @NonNull
    @PrimaryKey
    private String id = "";
    private String author; // user name
    private String name; // title
//    private String userID; // user who created the item.
    private String description;
    private long date;
    private String order;
    private String imageUrl;

    @Exclude
    private String imageLocalLocation;


    public Book() {

    }


    public Book(String id, String name, String author, String description, String image) {
        this.author = author;
        this.id = id;
        this.name = name;
        this.description = description;
        this.date = System.currentTimeMillis() / 1000L; //negetive date.
        this.order = Model.instance.getDescentOrderHash();

        this.imageUrl = image;
    }

    @Exclude
    public String getImageLocalLocation() {
        return imageLocalLocation;
    }

    @Exclude
    public void setImageLocalLocation(String imageLocalLocation) {
        this.imageLocalLocation = imageLocalLocation;
    }

//    public String getUserID() {
//        return userID;
//    }
//
//    public void setUserID(String userID) {
//        this.userID = userID;
//    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public String getAuthor() {
        return author;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @Override
    public String toString() {
        return "Name: " + this.name + ", Author: " + this.author + "ID: " + this.id;
    }
}
