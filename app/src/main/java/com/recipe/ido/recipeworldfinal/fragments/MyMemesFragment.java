package com.recipe.ido.recipeworldfinal.fragments;

import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.recipe.ido.recipeworldfinal.R;
import com.recipe.ido.recipeworldfinal.activities.WallActivity;
import com.recipe.ido.recipeworldfinal.data.Book;
import com.recipe.ido.recipeworldfinal.model.Model;

import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class MyMemesFragment extends Fragment {

    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 1;
    private MyAdapter adapter;

    public MyMemesFragment() {
    }


    public static MyMemesFragment newInstance(int columnCount) {
        MyMemesFragment fragment = new MyMemesFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }
    RecyclerView.OnScrollListener scrollListener;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("ListLog","List created!.");
        Model.instance.refresh(1);
        final View view = inflater.inflate(R.layout.fragment_wall_list, container, false);

        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            final RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            adapter = new MyAdapter(getActivity());
            recyclerView.setAdapter(adapter);
                    scrollListener = new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                            super.onScrollStateChanged(recyclerView, newState);
                            if (!recyclerView.canScrollVertically(1)) {
                                Log.d("Wall","Wall try load chunk..");
                                if (Model.instance.reachedToEnd) {
                                    stopScrollListener(recyclerView);
                                }
                                if (!Model.instance.isLoading) {
                                    Model.instance.getNextChunk();
                                    recyclerView.scrollToPosition(Model.instance.getLiveBookListCurrentSize() - 1);
                                }
                            }
                        }
                    };

                    recyclerView.addOnScrollListener(scrollListener);
            Model.instance.getLiveBookList().observe(this, new Observer<List<Book>>() {
                @Override
                public void onChanged(@Nullable List<Book> books) {
                    adapter.notifyDataSetChanged();
                    Log.d("ListLog", "Notify data changed.");
                }
            });
        }


        Model.instance.setListenToTouchedMeme(new Model.MemeTouchListener() {
            @Override
            public void onTouch(Book meme) {
                Log.d("ListLog", "Touched MEME!!@#$" + meme);
                NewBookFragment fragment = new NewBookFragment();
                Fragment a = new Fragment();
                FragmentTransaction tran = getActivity().getSupportFragmentManager().beginTransaction();
                tran.replace(R.id.wall_container, fragment);
                tran.addToBackStack("wall");
                tran.commit();
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Model.instance.getNextChunk();
    }

    private void stopScrollListener(RecyclerView rv) {
        rv.removeOnScrollListener(scrollListener);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Model.instance.lastTouchedMeme = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Book item);
    }
}
