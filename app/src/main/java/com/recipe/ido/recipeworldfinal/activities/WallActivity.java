package com.recipe.ido.recipeworldfinal.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.recipe.ido.recipeworldfinal.R;
import com.recipe.ido.recipeworldfinal.fragments.MyMemesFragment;
import com.recipe.ido.recipeworldfinal.fragments.NewBookFragment;
import com.recipe.ido.recipeworldfinal.fragments.WallFragment;
import com.recipe.ido.recipeworldfinal.model.Model;

import java.util.List;

public class WallActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private boolean isUserMenuOpen = false;
    private FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    private NavigationView navigationView;
    private WallFragment wFragment;
    private MyMemesFragment mwFragment;
    private final String MY_TITLE = "My Memes";
    private final String WALL_TITLE = "Memes World";

    private void refresh() {
        if (Model.instance.getState() == 0) {
            getSupportFragmentManager().beginTransaction().remove(wFragment).commit();
            startWallFragment();
        } else {
            startMyMemeWall();

        }

    }

    private void startMyMemeWall() {
        getSupportFragmentManager().popBackStack();
        mwFragment = new MyMemesFragment();
        FragmentTransaction tran = getSupportFragmentManager().beginTransaction();
        tran.replace(R.id.wall_container, mwFragment);
        tran.addToBackStack("");
        tran.commit();
    }

    private void startWallFragment() {
        wFragment = new WallFragment();
        FragmentTransaction tran = getSupportFragmentManager().beginTransaction();
        tran.replace(R.id.wall_container, wFragment);
        tran.commit();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wall);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setMyActionBar(WALL_TITLE);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NewBookFragment fragment = new NewBookFragment();
                FragmentTransaction tran = getSupportFragmentManager().beginTransaction();
                tran.add(R.id.wall_container, fragment);
                tran.addToBackStack("");
                tran.commit();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View navHeader = navigationView.getHeaderView(0);
        TextView email = (TextView)navHeader.findViewById(R.id.nav_bar_email);
        email.setText(user.getEmail());
        TextView name = (TextView)navHeader.findViewById(R.id.nav_bar_name);
        name.setText(user.getDisplayName());
        if (savedInstanceState == null) {
            startWallFragment();
        }

    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
                super.onBackPressed();
        }
        checkAndSetTitle();
    }

    public void checkAndSetTitle() {
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        for (Fragment f:fragments) {
            if (f instanceof WallFragment) {
                setMyActionBar(WALL_TITLE);
            } else if (f instanceof MyMemesFragment) {
                setMyActionBar(MY_TITLE);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.wall, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {//Refresh
            refresh();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_my_memes) {
            startMyMemeWall();
            setMyActionBar(MY_TITLE);
        }  else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_user_logout) {
            logout();
        } else if (id == R.id.nav_wall) {
            setMyActionBar(WALL_TITLE);
            if (Model.instance.getState() == 0) {
                refresh();
            } else {
                getSupportFragmentManager().popBackStack();
            }
        }



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void logout() {

        FirebaseAuth.getInstance().signOut();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(intent.FLAG_ACTIVITY_NEW_TASK | intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void setMyActionBar(String title) {
        getSupportActionBar().setTitle(title);
    }

    public void nameClicked(View v) {
        ImageView arrow = (ImageView)findViewById(R.id.arrow_drop);
        navigationView.getMenu().clear();
        if (isUserMenuOpen) {
            navigationView.inflateMenu(R.menu.activity_wall_drawer);
            isUserMenuOpen = false;
            arrow.setImageResource(R.drawable.ic_arrow_down);
        } else {
            arrow.setImageResource(R.drawable.ic_arrow_drop_up);
            navigationView.inflateMenu(R.menu.activity_wall_drawer_user);
            isUserMenuOpen = true;
        }

    }
}
