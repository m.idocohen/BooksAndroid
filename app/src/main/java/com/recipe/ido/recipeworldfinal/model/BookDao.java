package com.recipe.ido.recipeworldfinal.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.recipe.ido.recipeworldfinal.data.Book;

import java.util.List;

@Dao
public interface BookDao {
    @Query("select * from Book")
    List<Book> getAll();



    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Book... books);

    @Delete
    void delete(Book book);

    @Query("DELETE FROM Book")
    void deleteAllBooks();
}
