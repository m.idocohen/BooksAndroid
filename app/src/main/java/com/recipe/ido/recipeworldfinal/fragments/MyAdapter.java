package com.recipe.ido.recipeworldfinal.fragments;


import android.app.Activity;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.recipe.ido.recipeworldfinal.R;
import com.recipe.ido.recipeworldfinal.data.Book;
import com.recipe.ido.recipeworldfinal.model.Model;

import java.util.Calendar;

class LoadingViewHolder extends RecyclerView.ViewHolder {
    public ProgressBar progressBar;

    public LoadingViewHolder(View itemView) {
        super(itemView);
        progressBar = (ProgressBar)itemView.findViewById(R.id.list_progress);
    }
}

class ItemViewHolder extends RecyclerView.ViewHolder {
    public final View mView;
    public final TextView mTitle;
    public final TextView mDescription;
//    public final TextView mDate;
    public final TextView mUserName;
    public final ImageView mImage;
    public final View mCard;
    public Book book;

    public ItemViewHolder(View itemView) {
        super(itemView);
        mView = itemView;
        mTitle = (TextView) itemView.findViewById(R.id.card_title);
//        mDate = (TextView) itemView.findViewById(R.id.date);
        mUserName = (TextView) itemView.findViewById(R.id.user_name);
        mDescription = (TextView) itemView.findViewById(R.id.card_description);
        mImage = (ImageView) itemView.findViewById(R.id.thumbnail);
        mCard =  itemView.findViewById(R.id.card_view);
    }


}


public class MyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int VIEW_TYPE_BOOK = 0, VIEW_TYPE_LOADIN = 1;
    boolean isLoading;
    Activity activity;


    public MyAdapter(Activity activity) {
        this.activity = activity;
    }

    @Override
    public int getItemViewType(int position) {
        return Model.instance.getLiveBookList().getValue().get(position) == null? VIEW_TYPE_LOADIN:VIEW_TYPE_BOOK;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_BOOK) {
            View view = LayoutInflater.from(activity)
                    .inflate(R.layout.meme_card, parent, false);
            return new ItemViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADIN) {
            View view = LayoutInflater.from(activity)
                    .inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof  ItemViewHolder) { // Set Book loader
            final Book book = Model.instance.getLiveBookList().getValue().get(position);
            final ItemViewHolder viewHolder = (ItemViewHolder) holder;
            viewHolder.mTitle.setText(book.getName());
            viewHolder.mUserName.setText(book.getAuthor());
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(book.getDate());
            int year = cal.get(Calendar.YEAR);
            int day = cal.get(Calendar.DAY_OF_MONTH);
            int month = cal.get(Calendar.MONTH);
            String date = day + "/" + month + "/" + year;
//            viewHolder.mDate.setText(date);
            viewHolder.mDescription.setText(book.getDescription());
            Model.instance.getImage(book.getImageUrl(), new Model.GetImageListener() {
                @Override
                public void onDone(Bitmap imageBitmap) {
                    viewHolder.mImage.setImageBitmap(imageBitmap);
                }
            });
            View.OnClickListener listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Model.instance.memeTouched(position);
                    Log.d("Adapter", "meme touched"+position);
                }
            };
            viewHolder.mCard.setOnClickListener(listener);
            viewHolder.mImage.setOnClickListener(listener);
        } else if (holder instanceof LoadingViewHolder) { // Set loader item
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return Model.instance.getLiveBookList().getValue().size();
    }

    public void setLoaded() {
        isLoading = false;
    }
}
