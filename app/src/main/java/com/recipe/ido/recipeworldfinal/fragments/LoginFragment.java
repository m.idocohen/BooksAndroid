package com.recipe.ido.recipeworldfinal.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.recipe.ido.recipeworldfinal.R;
import com.recipe.ido.recipeworldfinal.activities.LoginActivity;
import com.recipe.ido.recipeworldfinal.activities.WallActivity;


public class LoginFragment extends android.support.v4.app.Fragment  {

    private OnFragmentInteractionListener mListener;
    private FirebaseAuth mAuth;
    private View progress;
    private View form;

    public LoginFragment() {
        // Required empty public constructor
    }


    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mAuth = FirebaseAuth.getInstance();
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        form =  view.findViewById(R.id.login_form);
        progress =  view.findViewById(R.id.login_progress);

        Button gotoRegisterBtn = (Button)view.findViewById(R.id.goto_register_button);
        Button loginBtn = (Button)view.findViewById(R.id.sign_in_button);
        final AutoCompleteTextView mEmailView = (AutoCompleteTextView) view.findViewById(R.id.email);
        final EditText mPasswordView = (EditText) view.findViewById(R.id.password);

        gotoRegisterBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoRegister(mEmailView.getText().toString(), mPasswordView.getText().toString());
            }
        });

        loginBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onLogin(mEmailView.getText().toString(), mPasswordView.getText().toString());
            }
        });

        return view;
    }

    private void onLogin(String email, String password) {
        showProgress(true);
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("LoginTag", "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            showProgress(false);
                            // If sign in fails, display a message to the user.
                            Log.w("LoginTag", "signInWithEmail:failure", task.getException());
                            Toast.makeText(getActivity(), "Failed: "+task.getException().getMessage(),
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                    }
                });
    }
    private void updateUI(FirebaseUser user) {
        if (user != null) { // Check if user logged in
            ((LoginActivity)getActivity()).startWallActivity();
        }
    }

    public void showProgress(Boolean show) {
        if (show) {
            progress.setVisibility(View.VISIBLE);
            form.setVisibility(View.GONE);
        } else {
            progress.setVisibility(View.GONE);
            form.setVisibility(View.VISIBLE);
        }
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public void gotoRegister(String email,String password) {
        Bundle bundle = new Bundle();
        bundle.putString("email", email);
        bundle.putString("password", password);
            RegisterFragment fragment = new RegisterFragment();
            fragment.setArguments(bundle);
            FragmentManager manager = getFragmentManager();
            FragmentTransaction tran = manager.beginTransaction();
            tran.replace(R.id.login_container, fragment);
            tran.addToBackStack(null);
            tran.commit();
            Log.d("LoginLog", "Goto reg");
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
