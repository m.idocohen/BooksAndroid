package com.recipe.ido.recipeworldfinal.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.recipe.ido.recipeworldfinal.R;
import com.recipe.ido.recipeworldfinal.activities.LoginActivity;
import com.recipe.ido.recipeworldfinal.activities.WallActivity;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RegisterFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class RegisterFragment extends android.support.v4.app.Fragment {

    private OnFragmentInteractionListener mListener;
    private FirebaseAuth mAuth;
    private AutoCompleteTextView mNameView;
    private Button registerBtn;
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View progress;
    private View form;

    public RegisterFragment() {
        // Required empty public constructor
    }

    public void showProgress(Boolean show) {
        if (show) {
            progress.setVisibility(View.VISIBLE);
            form.setVisibility(View.GONE);
        } else {
            progress.setVisibility(View.GONE);

            form.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        registerBtn = (Button)view.findViewById(R.id.register_button);
        mEmailView = (AutoCompleteTextView) view.findViewById(R.id.email);
        mNameView = (AutoCompleteTextView) view.findViewById(R.id.full_name);
        mPasswordView = (EditText) view.findViewById(R.id.password);
        progress =  view.findViewById(R.id.register_progress);
        form =  view.findViewById(R.id.register_form);
        mPasswordView.setText(getArguments().getString("password"));
        mEmailView.setText(getArguments().getString("email"));
        mNameView.requestFocus();
        mAuth = FirebaseAuth.getInstance();
        registerBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                registerNewUser(mNameView.getText().toString(), mEmailView.getText().toString(), mPasswordView.getText().toString());
            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void registerNewUser(String displayName, String email, String password) {
        if (displayName.isEmpty() || email.isEmpty() || password.isEmpty()){
            Toast.makeText(getActivity(), "All feilds are required.",
                    Toast.LENGTH_SHORT).show();
        } else {
            showProgress(true);
            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d("RegisterLog", "createUserWithEmail:success");
                                FirebaseUser user = mAuth.getCurrentUser();
                                setProfile(user);
                            } else {
                                showProgress(false);
                                // If sign in fails, display a message to the user.
                                Log.w("RegisterTag", "createUserWithEmail:failure", task.getException());
                                Toast.makeText(getActivity(), "failed, " + task.getException().getMessage(),
                                        Toast.LENGTH_SHORT).show();
                            }


                        }
                    });
        }

    }



    private void setProfile(FirebaseUser user) {
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(mNameView.getText().toString())
                .build();
        user.updateProfile(profileUpdates)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            ((LoginActivity)getActivity()).startWallActivity();
                        } else {
                            showProgress(false);
                        }
                    }
                });
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
