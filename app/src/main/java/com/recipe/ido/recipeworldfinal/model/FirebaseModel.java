package com.recipe.ido.recipeworldfinal.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.recipe.ido.recipeworldfinal.data.Book;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class FirebaseModel {
    final int CHUNK_SIZE = 4;
    public String lastOrder = "";
    public String myListLastOrder = "";

    interface StringListener {
        void onDone(String string);
    }
    public interface ImageListener {
        void onDone(Bitmap image);
    }

    interface GetBookChunkListener{
        public void onSuccess(List<Book> booksList);
        public void onFailed();
    }

    interface getLatestAddedBookListener {
        public void onSuccess(Book book);
    }
    Book book;
    public void booksChangedListener(final getLatestAddedBookListener listener) {
        DatabaseReference fbDbr = FirebaseDatabase.getInstance().getReference().child("books");
        Log.d("FBModel", "Set Changed listener");

        fbDbr.orderByChild("order").limitToFirst(1).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot stSnapshot: dataSnapshot.getChildren()) {
                    book =  stSnapshot.getValue(Book.class);
                    Log.d("FBModel", "Value is: " + book);
                    listener.onSuccess(book);
                }
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("FBModel", "Failed to read value.", error.toException());
            }
        });
    }

    public void delete(String memeId) {
        final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        final FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        mDatabase.child("books").child(memeId).removeValue();
        mDatabase.child("users").child(currentUser.getUid()).child("books").child(memeId).removeValue();
    }


    List<Book> books;
    ValueEventListener eventListener;
    public void getChunkOfBooks(final GetBookChunkListener listener) {

        DatabaseReference fbDbr = FirebaseDatabase.getInstance().getReference().child("books");
        Log.d("FBModel", "try to get chunk!");
        eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                books = new LinkedList<Book>();
                for (DataSnapshot stSnapshot: dataSnapshot.getChildren()) {
                    book = stSnapshot.getValue(Book.class);
                    Log.d("FBModel", "Value is: " + book);
                    lastOrder = book.getOrder();
                    books.add(book);
                }

                listener.onSuccess(books);
                Log.d("FBModel",Model.instance.getLiveBookList().getValue().size() + "");
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.d("FBModel", "get chunk cancel!!!");
                listener.onFailed();


            }
        };
        fbDbr.orderByChild("order").startAt(this.lastOrder).limitToFirst(CHUNK_SIZE + 1).addListenerForSingleValueEvent(eventListener);
        checkConnection();
    }

    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    DatabaseReference fbDbr = FirebaseDatabase.getInstance().getReference().child("users").child(user.getUid()).child("books");
    List<Book> myMemes;
    ValueEventListener eventMyMemeListener;
    public void getChunkOfMyMemes(final GetBookChunkListener listener) {

        Log.d("FBModel", "try to get chunk!");
        eventMyMemeListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                myMemes = new LinkedList<Book>();
                for (DataSnapshot stSnapshot: dataSnapshot.getChildren()) {
                    book = stSnapshot.getValue(Book.class);
                    Log.d("FBModel", "my meme Value is: " + book);
                    lastOrder = book.getOrder();
                    myMemes.add(book);
                }

                listener.onSuccess(myMemes);
                Log.d("FBModel",Model.instance.getLiveBookList().getValue().size() + "");
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.d("FBModel", "get chunk cancel!!!");
                listener.onFailed();


            }
        };

        fbDbr.orderByChild("order").startAt(this.lastOrder).limitToFirst(CHUNK_SIZE + 1).addListenerForSingleValueEvent(eventMyMemeListener);
        checkConnection();
    }




    private static boolean firstLoad = true;
    public void checkConnection() {
        if (firstLoad) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        firstLoad = false;
        DatabaseReference connectedRef = FirebaseDatabase.getInstance().getReference(".info/connected");
        connectedRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (connected) {
                    Log.d("FBModel", "connected!!!");

                } else {
                    Log.d("FBModel", "not connected!!!");
                    eventListener.onCancelled(null);
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                System.err.println("Listener was cancelled");
            }
        });
    }

    public void addMeme(final Book meme, Bitmap bm){
        final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        final FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();


            saveImage(bm, new StringListener() {
                @Override
                public void onDone(String url) {
                    Log.d("fbModel","ID: " + meme.getId());
                    if (meme.getId().equals("")) {
                        meme.setId(mDatabase.push().getKey());
                    }
                    meme.setImageUrl(url);
//                book.setUserID(currentUser.getUid());
                    mDatabase.child("books").child(meme.getId()).setValue(meme);
                    mDatabase.child("users").child(currentUser.getUid()).child("books").child(meme.getId()).setValue(meme);
                }
            });


    }

    //Managing Files
    public void saveImage(Bitmap imageBitmap, final StringListener listener) {
        FirebaseStorage storage = FirebaseStorage.getInstance();

        Date d = new Date();
        String name = ""+ d.getTime();
        StorageReference imagesRef = storage.getReference().child("images").child(name);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Log.d("FBModel", "IMAGE SIZE:" + imageBitmap.getByteCount());
        if (imageBitmap.getByteCount() > 8540000) {
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 8, baos);
        } else {
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 15, baos);
        }
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = imagesRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception exception) {
                listener.onDone(null);
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                @SuppressWarnings("VisibleForTests") Uri downloadUrl = taskSnapshot.getDownloadUrl();
                listener.onDone(downloadUrl.toString());
            }
        });
    }


    public void getImage(String url, final ImageListener listener){
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference httpsReference = storage.getReferenceFromUrl(url);
        final long ONE_MEGABYTE = 1024 * 1024;
        httpsReference.getBytes(3* ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Bitmap image = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
                Log.d("TAG","get image from firebase success");
                listener.onDone(image);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception exception) {
                Log.d("TAG",exception.getMessage());
                Log.d("TAG","get image from firebase Failed");
                listener.onDone(null);
            }
        });
    }

}
