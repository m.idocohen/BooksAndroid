package com.recipe.ido.recipeworldfinal.model;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;
import android.webkit.URLUtil;

import com.google.firebase.auth.FirebaseAuth;
import com.recipe.ido.recipeworldfinal.data.Book;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.List;

public class Model {
    public Book lastTouchedMeme;
    public static Model instance = new Model();
    public static FirebaseModel fbModel = new FirebaseModel();
    private MyLiveList liveWallBookList = new LiveMemeList();
    private int state = 0; //0-wall, 1-my memes
    public boolean isLoading = false;
    private boolean firstLoad = true;
    private String lastLoadedID = "";
    private String myLastLoadedID = "";
    public boolean reachedToEnd = false;
    public boolean reachedToEndOfMyMemes = false;
    private Model() {
    }

    public int getState() {
        return state;
    }
    public void refresh(int state) {
        isLoading = false;
        firstLoad = true;
        lastLoadedID = "";
        reachedToEnd = false;
        fbModel.lastOrder = "";
        liveWallBookList.refresh();
        this.state = state;
        if (state == 1) {
            liveWallBookList = new LiveMyMemeList();
        } else {
            liveWallBookList = new LiveMemeList();
        }
//        myLastLoadedID = "";



    }
    public void addMeme(Book book, Bitmap bm) {
        fbModel.addMeme(book, bm);
    }

    public void getNextChunk() {
        this.liveWallBookList.nextChunk();
    }

    public void deleteMeme(String id) {
        fbModel.delete(id);
    }


    public String getDescentOrderHash() {
        long now = System.currentTimeMillis();
        String lex = "abcdefghijkl";
        long index = 0;
        String order = "";
        while (now != 0) {
            index = 10 - now % 10;
            now = now / 10;
            order += lex.charAt((int)index);
        }


        return new StringBuilder(order).reverse().toString();
    }

    public String currentUserName() {
        return FirebaseAuth.getInstance().getCurrentUser().getDisplayName();
    }


    public interface MemeTouchListener {
        void onTouch(Book meme);
    }
    public void memeTouched(int position) {
        if (memeTouchListener != null && state == 1) {
            lastTouchedMeme = liveWallBookList.getValue().get(position);
            memeTouchListener.onTouch(lastTouchedMeme);
        }
    }

    MemeTouchListener memeTouchListener;
    public void setListenToTouchedMeme(MemeTouchListener listener) {
        memeTouchListener = listener;
    }

    interface LiveListInterface {
        void refresh();
        void nextChunk();

         }

         abstract class MyLiveList extends MutableLiveData<List<Book>> implements LiveListInterface{ }

    class LiveMemeList extends MyLiveList {
        private boolean isListening = false;
        @Override
        protected void onActive() {
            Log.d("FBModel","Active live list - set listener");
            if (!isListening) {
                isListening = true;
                super.onActive();
                fbModel.booksChangedListener(new FirebaseModel.getLatestAddedBookListener() {
                    @Override
                    public void onSuccess(Book book) { //Handle update of changed. Currently will append the item on top.
                        if (!firstLoad) {
                            List<Book> newList = LiveMemeList.super.getValue();
                            if (newList != null && book != null) {
                                Log.d("Model", "ID: " + book.getId());

                                for (Book b:newList) {
                                    if (b != null) {
                                        Log.d("Model", "second ID: " + b.getId());
                                        if (b.getId().equals(book.getId())) { // if that an object that we already have. don't add it.
                                            setValue(newList);
                                            return;
                                        }
                                    }
                                }
                            }
                            newList.add(0, book);
                            setValue(newList);
                        }

                    }
                });
            }


        }

        public void refresh() {
            setValue(new LinkedList<Book>());
        }

        public void nextChunk() {
            List<Book> currentList;
            if (!isLoading&&!reachedToEnd) {
                isLoading = true;
                if (state == 0) { // Wall
                    currentList = Model.instance.getLiveBookList().getValue();

                } else { //My memes
                    currentList = Model.instance.getLiveBookList().getValue();
                }
                currentList.add(null);//add null at the end.
                setValue(currentList);
                fbModel.getChunkOfBooks(new FirebaseModel.GetBookChunkListener() {
                    @Override
                    public void onSuccess(List<Book> bookslist) { //Handle update of changed. Currently will append the item on top.

                        List<Book> newList = LiveMemeList.super.getValue();
                        if (newList.size() > 0 && !(newList.get(newList.size()-1) instanceof Book)) {
                            newList.remove(newList.size()-1); //remove loader
                        }

                        Log.d("Model", "Remove last added.");
                        if (bookslist.size() > 0 ) {
                            if (lastLoadedID.equals(bookslist.get(bookslist.size()-1).getId())) {
                                Log.d("Model", "Reached to end..");
                                reachedToEnd = true;
                            }
                            lastLoadedID = bookslist.get(bookslist.size()-1).getId();

                            if (bookslist.size() >= 0 && !firstLoad) {
                                bookslist.remove(0);
                            }

                            newList.addAll(bookslist);
                            firstLoad = false;

                            BookAsynchDao.replaceAll(newList, new BookAsynchDao.BookAsynchDaoListener<Boolean>() {
                                @Override
                                public void onComplete(Boolean data) {
                                    Log.d("Model", "Saved in local storage.");
                                }
                            });
                        }
                        setValue(newList);
                        isLoading = false;
                    }

                    @Override
                    public void onFailed() {
                        final List<Book> newList = LiveMemeList.super.getValue();
                        if (newList != null && newList.size() > 0) {
                            newList.remove(newList.size()-1);
                        }
                        if (firstLoad) {
                            firstLoad = false;
                            BookAsynchDao.getAll(new BookAsynchDao.BookAsynchDaoListener() {
                                @Override
                                public void onComplete(Object data) {
                                    newList.addAll((List<Book>)data);
                                    setValue(newList);
                                    isLoading = false;
                                }
                            });
                        }
                        setValue(newList);

                    }
                });
            }
        }


        public LiveMemeList() {
            super();
            setValue(new LinkedList<Book>());
        }

    }








    class LiveMyMemeList extends MyLiveList{
        private boolean isListening = false;
        @Override
        protected void onActive() {
                super.onActive();
        }

        public void refresh() {
            setValue(new LinkedList<Book>());
        }

        public void nextChunk() {
            List<Book> currentList = Model.instance.getLiveBookList().getValue();
            if (!isLoading&&!reachedToEnd) {
                isLoading = true;
                currentList.add(null);//add null at the end.
                setValue(currentList);
                fbModel.getChunkOfMyMemes(new FirebaseModel.GetBookChunkListener() {
                    @Override
                    public void onSuccess(List<Book> bookslist) { //Handle update of changed. Currently will append the item on top.
                        Log.d("Model", "got new memes from db. my live list" + bookslist.size());
                        List<Book> newList = LiveMyMemeList.super.getValue();
                            if (!(newList.get(newList.size()-1) instanceof Book)) {
                                newList.remove(newList.size()-1); //remove loader
                            }


                        Log.d("Model", "Remove last added.");
                        if (bookslist.size() > 0 ) {
                            if (lastLoadedID.equals(bookslist.get(bookslist.size()-1).getId())) {
                                Log.d("Model", "Reached to end..");
                                reachedToEnd = true;
                            }
                            lastLoadedID = bookslist.get(bookslist.size()-1).getId();

                            if (!reachedToEnd) {
                                Log.d("Model", "Add new memes to list.");
                                newList.addAll(bookslist);
                            }
                            firstLoad = false;
                        }
                        setValue(newList);
                        isLoading = false;
                    }

                    @Override
                    public void onFailed() {
                        final List<Book> newList = LiveMyMemeList.super.getValue();
                        if (newList != null && newList.size() > 0) {
                            newList.remove(newList.size()-1);
                        }
                        setValue(newList);

                    }
                });
            }
        }


        public LiveMyMemeList() {
            super();
            setValue(new LinkedList<Book>());
        }

    }










    public LiveData<List<Book>> getLiveBookList() {
        return liveWallBookList;
    }

    public int getLiveBookListCurrentSize() {
        return liveWallBookList.getValue().size();
    }



    public interface GetImageListener{
        void onDone(Bitmap imageBitmap);
    }
    public void getImage(final String url, final GetImageListener listener ){
        Log.d("Model","Get image from firebase/local");

        String localFileName = URLUtil.guessFileName(url, null, null);
        final Bitmap image = loadImageFromFile(localFileName);
        if (image == null) {//if image not found - try downloading it from parse
            Log.d("Model","Image is null");
            fbModel.getImage(url, new FirebaseModel.ImageListener() {
                @Override
                public void onDone(Bitmap imageBitmap) {
                    if (imageBitmap == null) {
                        listener.onDone(null);
                    }else {
                        //2.  save the image localy
                        String localFileName = URLUtil.guessFileName(url, null, null);
                        Log.d("Model", "save image to cache: " + localFileName);
                        saveImageToFile(imageBitmap, localFileName);
                        //3. return the image using the listener
                        listener.onDone(imageBitmap);
                    }
                }
            });
        }else {
            Log.d("Model","OK reading cache image: " + localFileName);
            listener.onDone(image);
        }
    }

    // Store / Get from local memory
    private void saveImageToFile(Bitmap imageBitmap, String imageFileName){
        imageFileName = imageFileName.substring(0, imageFileName.length()-5);
        if (imageBitmap == null) return;
        try {
            File dir = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES);
            if (!dir.exists()) {
                dir.mkdir();
            }
            File imageFile = new File(dir, imageFileName);
            imageFile.createNewFile();

            OutputStream out = new FileOutputStream(imageFile);
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.close();

            //addPicureToGallery(imageFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Bitmap loadImageFromFile(String imageFileName){
        imageFileName = imageFileName.substring(0, imageFileName.length()-5);
        Bitmap bitmap = null;
        try {
            File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            File imageFile = new File(dir,imageFileName);
            Log.d("Model","Load from: " + imageFileName);
            InputStream inputStream = new FileInputStream(imageFile);
            bitmap = BitmapFactory.decodeStream(inputStream);
            Log.d("tag","got image from cache: " + imageFileName);
        } catch (FileNotFoundException e) {
//            e.printStackTrace();
        } catch (IOException e) {
//            e.printStackTrace();
        }
        return bitmap;
    }


}
