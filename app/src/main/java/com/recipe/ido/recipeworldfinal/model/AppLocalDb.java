package com.recipe.ido.recipeworldfinal.model;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;

import com.recipe.ido.recipeworldfinal.MyApplication;
import com.recipe.ido.recipeworldfinal.data.Book;

@Database(entities = {Book.class}, version = 1)
abstract class AppLocalDbRepository extends RoomDatabase {
    public abstract BookDao bookDao();
}

public class AppLocalDb{
    static public AppLocalDbRepository db = Room.databaseBuilder(MyApplication.context,
            AppLocalDbRepository.class,
            "booksDB.db").fallbackToDestructiveMigration().build();
}
